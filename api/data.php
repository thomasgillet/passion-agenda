<?php
/*
 * Copyright 2021-2022 Thomas GILLET
 *
 * This file is part of Passion Agenda.
 *
 * Passion Agenda is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 * If not, see <https://www.gnu.org/licenses/>
 */

const FILE_NAME = 'data.json';
const BACKUP_PERIOD = 'Y-m-d';

const DATA_OCCURRENCES_KEY = 'occurrences';
const DATA_CANCELLED_KEY = 'cancelled';
const DATA_ASSIGNEES_KEY = 'assignees';


/******** HEADERS ********/

// Default to error 500
http_response_code(500);


/******** GET ********/

/* Deactivated because replaced by direct call to JSON file to avoid using PHP */
/* TODO If reactivated, add code to handle '304 not modified' status */

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
	die('GET is not suported');
/*	
	response(200, 'application/json');
	
	if (file_exists(FILE_NAME)) {
		
		// disable reporting so that readfile() does not output anything in case of error
		//error_reporting(0);
		// output file content
		$res = @readfile(FILE_NAME);
		// if error, send http error
		if ($res === false) {
			response(500);
			die('Cannot read data file');
		}
		
	} else {
		
		echo '{}';
		
	} 
*/
}


/******** POST ********/

else if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$data = loadData(FILE_NAME, []);

	$input = loadInput();

	if ($assignees = $input['assignees'])
		swapAssignees($data, $assignees);

	if ($cancellations = $input['cancellations'])
		swapCancellations($data, $cancellations);

	cleanup($data);
	
	saveData(FILE_NAME, BACKUP_PERIOD, $data);

	response(204);
}


/******** DATA TRANSFORMS ********/

/* DATA
{
	"occurrences": {
		"2021-07-01T16:00:00": {
			"assignees": {
				"<name>": true,
				"<name>": false
			},
			cancelled: true
		}
	}
}
*/

/* INPUT
{
	"assignees": {
		"assignee": {
			"2021-07-01T16:00:00": true,
			"2021-07-01T17:00:00": false,
			"2021-07-01T18:00:00": null,
		}
	}
	"cancellations": {
		"2021-07-01T16:00:00": true,
		"2021-07-01T17:00:00": false
	}
}
*/

function swapAssignees(&$data, $input) {
	
	foreach ($input as $assignee => $_) {
		foreach ($_ as $ts => $state) {
			
			$root =& $data[DATA_OCCURRENCES_KEY][$ts][DATA_ASSIGNEES_KEY];
			
			if ($state === NULL)
				unset($root[$assignee]);
			else
				$root[$assignee] = $state;
			
		}
	}
	
}

function swapCancellations(&$data, $input) {
	
	foreach ($input as $ts => $state) {
		
		$root =& $data[DATA_OCCURRENCES_KEY][$ts];
		
		if ($state)
			$root[DATA_CANCELLED_KEY] = true;
		else
			unset($root[DATA_CANCELLED_KEY]);

	}
	
}

function cleanup(&$data) {
	
	$min = date('Y-m-d', time() - 30 * 24 * 3600);
	
	foreach ($data[DATA_OCCURRENCES_KEY] as $ts => $occ) {
		
		if ($ts < $min)
			unset($data[DATA_OCCURRENCES_KEY][$ts]);
		
	}

}


/******** DATA LOADING ********/

function loadInput() {
	
	return readJson('php://input');
	
}

function loadData($file, $default) {

	if (file_exists($file))
		return readJson($file);
	else
		return $default;

}

function saveData($file, $backupPeriod, $data) {
	
	$json = json_encode($data, JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
	if ($json === false) die('Cannot encode data');

	$res = file_put_contents($file, $json);
	if ($res === false) die("Cannot write file '$file'");

	$file = $file . '_' . date($backupPeriod);
	$res = file_put_contents($file, $json);
	if ($res === false) die("Cannot write file '$file'");
}

function readJson($file) {
	
	$json = file_get_contents($file);
	if ($json === false) die("Cannot read file '$file'");
	
	$data = json_decode($json, true);
	if ($data === NULL) die('Cannot decode data');
	
	return $data;
}


/******** UTILS ********/

/*
function toggleValue(&$arr, $val, $enabled) {
	if ($enabled) {
		if (!$arr || !in_array($val, $arr)) $arr[] = $val;
	} else if ($arr) {
		$i = array_search($val, $arr);
		if ($i !== false) array_splice($arr, $i, 1);
	}
}
*/

function response($statusCode, $contentType = null) {
	http_response_code($statusCode);
	$contentType && header("Content-Type: $contentType");
}