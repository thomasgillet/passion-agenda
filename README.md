# Passion Agenda

Pour gérer les inscriptions des bénévoles aux permanences, dans un atelier d'autoréparation de vélo !

# Déploiement

## Solution facile

Pour le front, utiliser le code pré-compilé, en copiant-collant le contenu du dossier `dist` sur le serveur. \
Pour le back, copier-coller le dossier `api` sur le serveur.

## Solution pour les geeks

Pour compiler soit même le front :
```bash
$ npm install
$ npm run build
```
Et le dossier `dist` contiendra tout ce qu'il faut.

# Détails techniques

## Le front

Single-page application en TypeScript avec [VueJS 2](https://v2.vuejs.org/).

Compilée avec NPM.

## Le back

Script en vanilla PHP (nécessite PHP >= 5.2.0 pour les fonctions JSON).

La configuration est stockée dans `config.json` (pas de front prévu, il faut directement éditer le fichier).

Les données sont stockées dans `data.json`. A chaque modification, une copie de sauvegarde est conservée (maximum une par jour). \
:warning: Pour l'instant, ces copies ne sont pas nettoyées automatiquement.

# Licence

Le code est distribué sous licence GNU AGPL 3.0
