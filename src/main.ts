/*
 * Copyright 2021-2022 Thomas GILLET
 *
 * This file is part of Passion Agenda.
 *
 * Passion Agenda is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 * If not, see <https://www.gnu.org/licenses/>
 */

import Vue from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { Settings } from 'luxon'

import App from './App.vue'
import router from './router'

// Vue config
Vue.config.productionTip = false
Vue.component('FaIcon', FontAwesomeIcon);

// Luxon config
Settings.defaultLocale = 'fr-FR';

// Debug
// Vue.prototype.$log = (...args: any[]) => { console.log(...args); return args[0]; };

new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
