/*
 *Copyright 2021-2022 Thomas GILLET
 *
 *This file is part of Passion Agenda.
 *
 *Passion Agenda is free software:
 *you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 *as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 *If not, see <https://www.gnu.org/licenses/>
 */

import Vue from 'vue'
import { DateTime } from 'luxon'
import { Api, GetData } from '@/model/api'
import { Week, Day, Occurrence } from '@/model/model'
import { createOccurrencesByWeek, createCancellationData } from '@/model/glue'


export const state = Vue.observable(new class State {

	public weeks: Week[] = [];
	public pendingCount: number = 0;

	private _data: GetData | undefined;
	private _pending: Occurrence[] | undefined;

	/*** Load ***/

	public async load(from: DateTime, to: DateTime): Promise<Week[]> {
		// Load data
		this._data = await Api.get();
		const mdl = createOccurrencesByWeek(this._data, 4);
		this.weeks = mdl.weeks;
		// Done
		return this.weeks;
	}

	/*** State ***/

	public toggle(occ: Occurrence) {
		occ.pending = !occ.pending;
		occ.cancelled = !occ.cancelled;
		toggleElement(this._pending ?? (this._pending = []), occ, occ.pending);
		this.pendingCount = this._pending.length;
	}

	public async save() {
		if (this._pending?.length) {
			await Api.set(
				createCancellationData(this._pending)
			);
			for (let occ of this._pending) {
				occ.pending = false;
			}
			this._pending = undefined;
			this.pendingCount = 0;
		}
	}

	public revert() {
		if (this._pending) {
			for (let occ of this._pending) {
				occ.pending = false;
				occ.cancelled = !occ.cancelled;
			}
			this._pending = undefined;
			this.pendingCount = 0;
		}
	}

});

/*** Utils ***/

function toggleElement<T>(arr: T[], el: T, state: boolean) {
	const i = arr.indexOf(el);
	if (state && i < 0)
		arr.push(el);
	if (!state && i >= 0)
		arr.splice(i, 1);
}
