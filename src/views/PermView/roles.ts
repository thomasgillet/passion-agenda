import {
	faKey as key,
	faTv as computer
} from '@fortawesome/free-solid-svg-icons'

export const roles = {
	computer: { icon: computer, desc: 'Je sais utiliser Garadin' },
	key:      { icon: key,      desc: "J'ai une clé de l'atelier" }
}

export { faStar as all } from '@fortawesome/free-solid-svg-icons'