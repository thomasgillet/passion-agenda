<!--
 Copyright 2021-2022 Thomas GILLET
 
 This file is part of Passion Agenda.
 
 Passion Agenda is free software:
 you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
 Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 If not, see <https://www.gnu.org/licenses/>
-->
<script>
import autofocus from '@/components/autofocus'
import { Assignee } from '@/model/model'
import { state } from './state'
import { roles, all as allRolesIcon } from './roles'

import { faArrowLeft as backIcon } from '@fortawesome/free-solid-svg-icons'

function simpleString(name) {
	return name && name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

function arrayEq(a, b) {
	return JSON.stringify(a) == JSON.stringify(b);
}

export default {

	directives: { autofocus },

	data: () => ({
		state,
		name: state.currentAssignee?.name,
		roles: state.currentAssignee?.roles || [],
		remember: state.remember,
		ROLES: roles
	}),

	computed: {
		
		normalizedName() {
			return simpleString(this.name);
		},
		
		normalizedRoles() {
			return this.roles.sort();
		},
		
		matchingAssignees() {
			return this.state.assignees
				.filter(a => simpleString(a.name).includes(this.normalizedName));
		},
		
		backIcon() { return backIcon; },
		allRolesIcon() { return allRolesIcon; }
	},

	methods: {
		select(ass) {
			this.name = ass.name;
			this.roles = ass.roles;
		},
		create() {
			const ass =
				this.state.assignees.find(a => simpleString(a.name) == this.normalizedName && arrayEq(a.roles, this.normalizedRoles))
				|| new Assignee(this.name, this.roles);
			this.state.goSelectWith(ass, this.remember);
		},
		cancel(ass) {
			this.state.goView();
		}
	}
}
</script>

<template>
	<div class="AssigneeSelector">

		<button class="cancel" @click="cancel()">
			<FaIcon :icon="backIcon"/>
		</button>
		
		<div class="menu">

			<div class="assignees" v-if="matchingAssignees.length">
				<div class="hint">Je suis peut-être :</div>
				<button class="item" v-for="ass in matchingAssignees" :key="ass.id" @click="select(ass)">
					{{ ass.name }}
					<FaIcon v-for="role in ass.roles" :key="role" :icon="ROLES[role].icon"/>
				</button>
			</div>
			
			<div class="roles">
				<div class="list">
					<label v-for="(role, key) in ROLES">
						<input type="checkbox" v-model="roles" :value="key"/>
						<span class="content"><span class="icon"><FaIcon :icon="role.icon" fixed-width/></span> {{ role.desc }}</span>
					</label>
				</div>
				<div class="all" v-if="roles.length == Object.keys(ROLES).length">
					<span class="icon all-roles"><FaIcon :icon="allRolesIcon"/></span>
				</div>
			</div>

			<div class="name">
				<!-- No v-model to avoid issue with composition on mobile: https://github.com/vuejs/vue/issues/9777#issuecomment-478831263 -->
				<input
					class="input"
					v-autofocus
					:value="name"
					@input="name = $event.target.value"
					placeholder="Mon nom"
					spellcheck="false"
					autocomplete="off"/>
			</div>
			
		</div>
		
		<div class="submit">
			<label class="remember">
				<input type="checkbox" v-model="remember"/> Se souvenir de moi
			</label>
			<button @click="create()">
				C'est parti !
			</button>
		</div>

	</div>
</template>

<style scoped>

.AssigneeSelector {
	display: flex;
	flex-flow: column nowrap;
	padding: 1rem;
	color: #EEE;
}
.menu {
	display: flex;
	flex-flow: column nowrap;
}
.cancel {
	align-self: flex-start;
	margin-bottom: auto;
	font-size: 130%;
}
@media screen and (min-width: 541px) {
	.menu {
		flex-direction: column-reverse;
	}
	.cancel {
		display: none;
	}
}

.assignees {
	flex: 0 1 auto;
	overflow-y: auto;
	border: 1px #444;
	border-style: solid none;
	margin: 0 -1rem;
	padding: 0 .5rem;
	margin-bottom: .5em;
	padding-bottom: .5em;
}
.assignees .hint {
	padding: .4rem .8rem;
	font-size: 75%;
	color: #888;
}
.assignees .item {
	display: block;
	width: 100%;
	padding: .4rem .8rem;
	text-align: left;
	border-radius: 5px;
}
.assignees .item:hover, button.item:focus {
	background: var(--app-accent-color);
}
.assignees .item > svg {
	margin-left: .3em;
	font-size: 80%;
}

.roles {
	margin: .6rem 0;
	display: flex;
	align-items: center;
}
.roles .list {
	flex: 1;
	display: flex;
	flex-flow: column nowrap;
	align-items: flex-start;
}
.roles label {
	margin: .3em 0;
}
.roles .content {
	display: inline-block;
	border-radius: 100px;
	padding-right: .8em;
}
.roles .icon {
	display: inline-flex;
	align-items: center;
	justify-content: center;
	width: 2em;
	height: 2em;
	vertical-align: middle;
	border-radius: 100px;
	font-size: 90%;
}
.roles input {
	display: none;
}
.roles .content:hover {
	background: var(--app-accent-color);
}
.roles .icon {
	color: #FFF;
	background: #444;
}
.roles input:checked + .content > .icon,
.roles .all-roles {
	color: #FFF;
	background: var(--app-accent-color);
	border: solid 2px;
}

.name {
	display: flex;
}
.name > input {
	width: 100%;
}
.name > input::placeholder {
	font-style: italic;
	opacity: .4;
}

.submit {
	display: flex;
	align-items: center;
	margin-top: .5rem;
}
.submit > button {
	padding: .5em 1em;
	border-radius: 100px;
	background: var(--app-accent-color);
}
.submit > button:hover {
	color: var(--app-accent-color);
	background: #FFF;
}
.submit > .remember {
	margin-right: auto;
	font-size: 60%;
}
.submit > .remember > input {
	vertical-align: middle;
}

label {
	cursor: pointer;
}

</style>
