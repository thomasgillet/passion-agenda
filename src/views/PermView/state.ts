/*
 *Copyright 2021-2022 Thomas GILLET
 *
 *This file is part of Passion Agenda.
 *
 *Passion Agenda is free software:
 *you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 *as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 *If not, see <https://www.gnu.org/licenses/>
 */

import Vue from 'vue'
import { DateTime } from 'luxon';
import { Api, GetData } from '@/model/api'
import { Week, Occurrence, Assignee } from '@/model/model'
import { createOccurrencesByWeek, createAssigneeData } from '@/model/glue'

const STORAGE_ASSIGNEE_NAME = 'agendaAssigneeName';

export const state = Vue.observable(new class State {

	public weeks: Week[] = [];
	public assignees: Assignee[] = [];
	public remember: boolean = false;

	public viewing = true;
	public editing = false;
	public selecting = false;

	private _data: GetData | undefined;

	/*** Load ***/

	public async load(): Promise<Week[]> {
		// Load data
		this._data = await Api.get();
		// Compute model
		const mdl = createOccurrencesByWeek(this._data, 4);
		this.weeks = mdl.weeks;
		this.assignees = mdl.assignees;
		// Update selection
		for (const week of this.weeks)
		for (const day of week.days)
		for (const occ of day.occurrences) {
			occ.selected = contains(occ.assignees, this.currentAssignee);
		}
		// Done
		return this.weeks;
	}

	/*** State ***/

	public goView() {
		this.revertToggledOccurrences();
		this.resetCurrentAssignee();
		this.viewing = true;
		this.editing = false;
		this.selecting = false;
	}

	public goEdit() {
		this.viewing = false;
		this.editing = true;
		this.selecting = false;
	}

	public goSelectWithCurrent() {
		// Already selected
		if (this.currentAssignee) {
			this.setCurrentAssignee(this.currentAssignee);
			this.goSelect();
			return;
		}
		// Remembered
		const name = localStorage?.getItem(STORAGE_ASSIGNEE_NAME);
		if (name) {
			this.setCurrentAssignee(this.assignees.find(ass => ass.name == name) || new Assignee(name));
			this.remember = true;
			this.goSelect();
			return;
		}
		// No assignee
		this.goEdit();
	}

	public goSelectWith(assignee: Assignee, remember: boolean) {
		// State
		this.setCurrentAssignee(assignee);
		this.remember = remember;
		// Store
		if (remember)
			localStorage.setItem(STORAGE_ASSIGNEE_NAME, assignee.name);
		else
			localStorage.removeItem(STORAGE_ASSIGNEE_NAME);
		// Go
		this.goSelect();
	}

	private goSelect() {
		this.viewing = false;
		this.editing = false;
		this.selecting = true;
	}

	public goSave() {
		this.saveToggledOccurrences();
		this.resetCurrentAssignee();
		this.viewing = true;
		this.editing = false;
		this.selecting = false;
	}

	/*** Selection ***/

	public currentAssignee: Assignee | undefined;

	public toggledOccurrences: Occurrence[] = [];

	private setCurrentAssignee(assignee: Assignee): void {
		// Set assignee
		this.currentAssignee = assignee;
		if (!this.assignees.includes(assignee))
			this.assignees.push(assignee);
		// Update occurrences
		for (const week of this.weeks)
		for (const day of week.days)
		for (const occ of day.occurrences) {
			if (occ.pending) {
				occ.pending = contains(occ.assignees, this.currentAssignee) != occ.selected;
				toggleElement(this.toggledOccurrences, occ, occ.pending);
			} else {
				occ.selected = contains(occ.assignees, this.currentAssignee);
			}
		}
		// Update assignees
		for (const ass of this.assignees) {
			ass.selected = ass === this.currentAssignee;
		}
	}

	private resetCurrentAssignee(): void {
		// Update assignees
		for (const ass of this.assignees) {
			ass.selected = false;
		}
	}

	/*public toggleDayOccurrences(day: Day) {
		for (const occ of day.occurrences)
			if (occ.instant.isValid)
				this.toggleOccurrence(occ);
	}*/

	public toggleOccurrence(occurrence: Occurrence) {
		occurrence.selected = !occurrence.selected;
		occurrence.pending = !occurrence.pending;
		toggleElement(this.toggledOccurrences, occurrence, occurrence.pending);
		toggleElement(occurrence.assignees, this.currentAssignee, occurrence.selected);
	}

	public async saveToggledOccurrences() {
		// Check
		if (!this.currentAssignee)
			throw new Error('Not in selection mode');
		// Send request
		await Api.set(
			createAssigneeData(this.currentAssignee, this.toggledOccurrences)
		);
		// Reset
		for (const occ of this.toggledOccurrences) {
			occ.pending = false;
		}
		this.toggledOccurrences = [];
	}

	public revertToggledOccurrences() {
		for (const occ of this.toggledOccurrences) {
			occ.selected = !occ.selected;
			occ.pending = false;
			toggleElement(occ.assignees, this.currentAssignee, occ.selected);
		}
		this.toggledOccurrences = [];
	}
});

/*** Utils ***/

function contains<T>(arr: T[], el: T) {
	return arr.indexOf(el) >= 0;
}

function toggleElement<T>(arr: T[], el: T, state: boolean) {
	const i = arr.indexOf(el);
	if (state && i < 0)
		arr.push(el);
	if (!state && i >= 0)
		arr.splice(i, 1);
}
