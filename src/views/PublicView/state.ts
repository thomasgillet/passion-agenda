/*
 *Copyright 2021-2022 Thomas GILLET
 *
 *This file is part of Passion Agenda.
 *
 *Passion Agenda is free software:
 *you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 *as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 *If not, see <https://www.gnu.org/licenses/>
 */

import Vue from 'vue'
import { Api } from '@/model/api'
import { Week } from '@/model/model'
import { createOccurrencesByWeek } from '@/model/glue'

export const state = Vue.observable(new class State {

	public weeks: Week[] = [];

	/*** Load ***/

	public async load(): Promise<Week[]> {
		// Load data
		const data = await Api.get();
		// Compute model
		const mdl = createOccurrencesByWeek(data, 2);
		this.weeks = mdl.weeks;
		// Done
		return this.weeks;
	}
});