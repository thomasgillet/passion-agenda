/*
 * Copyright 2021-2022 Thomas GILLET
 *
 * This file is part of Passion Agenda.
 *
 * Passion Agenda is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 * If not, see <https://www.gnu.org/licenses/>
 */

 import { DateTime } from 'luxon'

export class HasInstant {
	constructor (public readonly instant: DateTime) {}
	valueOf() { return this.instant.valueOf() }
}

export class Assignee {

	public readonly id: string;
	public selected = false;
	
	constructor(
		public readonly name: string,
		public readonly roles: string[] = []
	) {
		this.roles.sort();
		this.id = [this.name, ...this.roles].join('┃');
	}
	
	public toString() {
		return this.id;
	}
	
	public static fromString(str: string) {
		const tkns = str.split(/┃/g);
		return new Assignee(tkns.splice(0, 1)[0], tkns)
	}
}

export class Occurrence extends HasInstant {

	public assignees: Assignee[] = [];
	public cancelled: boolean = false;
	public selected: boolean = false;
	public pending: boolean = false;

	constructor (instant: DateTime) {
		super(instant);
	}
}

export class Day extends HasInstant {

	public readonly occurrences: Occurrence[] = [];

	constructor (instant: DateTime) {
		super(instant.startOf('day'));
	}

	hasSame(instant: DateTime) {
		return this.instant.hasSame(instant, 'day');
	}

}

export class Week extends HasInstant {

	public readonly days: Day[] = [];

	constructor (instant: DateTime) {
		super(instant.startOf('week'));
	}

	hasSame(instant: DateTime) {
		return this.instant.hasSame(instant, 'week');
	}
}

export class Month extends HasInstant {

	public readonly weeks: Week[] = [];

	constructor (instant: DateTime) {
		super(instant.startOf('month'));
	}

	hasSame(instant: DateTime) {
		return this.instant.hasSame(instant, 'year') && this.instant.hasSame(instant, 'month');
	}
}

