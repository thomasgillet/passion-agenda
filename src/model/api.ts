/*
 * Copyright 2021-2022 Thomas GILLET
 *
 * This file is part of Passion Agenda.
 *
 * Passion Agenda is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Passion Agenda is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Passion Agenda.
 * If not, see <https://www.gnu.org/licenses/>
 */

import { DateTime } from 'luxon'


/******** API MODEL ********/

export type WeekDayValue = 1 | 2 | 3 | 4 | 5 | 6 | 7;

export interface WeeklySlot {
	weekday: WeekDayValue
	hour: number
	minute: number
	assignees?: string[]
}

export interface WeeklyOccurrence {
	assignees?: {
		[name: string]: boolean
	}
	cancelled?: boolean
}

export interface GetData {
	slots: WeeklySlot[]
	occurrences: {
		[instant: string]: WeeklyOccurrence
	}
}

export interface SetData {
	assignees?: {
		[assignee: string]: {
			[instant: string]: boolean | null
		}
	},
	cancellations?: {
		[instant: string]: boolean
	}
}


/******** API SERVICE ********/

const API_CFG = 'api/conf.json';
const API_GET = 'api/data.json';
const API_SET = 'api/data.php';

export const Api = {

	async get(): Promise<GetData> {

		const [ config, data ] = await Promise.all([
			httpGetJson(API_CFG, false),
			httpGetJson(API_GET, true, {})
		]);

		return {
			slots: config.slots,
			occurrences: data.occurrences
		};
	},

	set(data: SetData): Promise<Response> {
		return httpPostJson(API_SET, data);
	},

	dt2str(instant: DateTime) {
		return instant.toISO({ suppressMilliseconds: true, includeOffset: false });
	},

	str2dt(str: string): DateTime {
		return DateTime.fromISO(str);
	}

};


/******** UTILS ********/

function httpGetJson(url: string, noCache: boolean, defaultValue?: any) {
	
	let init: any = undefined;
	
	if (noCache) {
		const headers = new Headers();
		headers.append('Cache-Control', 'no-cache');
		init = { headers };
	}
	
	return fetch(url, init)
		.then(r => {
			if (Math.floor(r.status / 100) == 2) return r.json();
			if (defaultValue !== undefined && r.status == 404) return defaultValue;
			return Promise.reject(r);
		});
}

function httpPostJson(url: string, data: any) {
	return fetch(url, { method: 'POST', body: JSON.stringify(data) })
		.then(r => {
			if (Math.floor(r.status / 100) == 2) return r;
			return Promise.reject(r);
		});
}
