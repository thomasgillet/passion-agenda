module.exports = {

	publicPath: '',

	chainWebpack: config => {
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap(options => {
        options.compilerOptions.preserveWhitespace = true;
        return options;
      });
  },

	devServer: {
		proxy: {
			'/api': {
				target: 'http://changedechaine.free.fr'
			}
		}
	}
}
